# Welcome to my Data Automation CDK project[s]

Here you will learn to use CDK to deploy Serverless data integration services  (it easy to discover, prepare, and combine data for analytics, and application development)
1. ## 🧰 Prerequisites

   - 🛠 AWS CLI Installed & Configured - [Get help here](https://youtu.be/TPyyfmQte0U)
   - 🛠 AWS CDK Installed & Configured - [Get help here](https://www.youtube.com/watch?v=MKwxpszw0Rc)
   - 🛠 Python Packages, _Change the below commands to suit your operating system, the following are written for \_Amazon Linux 2_
     - Python3 - `yum install -y python3`
     - Python Pip - `yum install -y python-pip`
     - Virtualenv - `pip3 install virtualenv`

2. #### Use Visualstudio code (recommended by AWS )

3. ## Technologies

    - AWS GLUE (Database ,Crawler,Job, Workflow)
    - AWS Lambda
    - AWS S3
    - Docker Image
    - Gitlab

4. ## Documentation for the Project

 * [AWS GLUE](https://docs.aws.amazon.com/cdk/api/latest/)          
 * [AWS Lambda](https://docs.aws.amazon.com/lambda/index.html)       
 * [AWS S3](https://docs.aws.amazon.com/s3/index.html)
 * [AWS CDK](https://docs.aws.amazon.com/cdk/api/latest/)
 * [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
 

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

Enjoy!
